#!/usr/bin/env bash

echo "[INFO] Going to find route tables names"

tables=$(
    ip route show table all |
        grep "table" |
        sed 's/.*\(table.*\)/\1/g' |
        awk '{print $2}' |
        sort | uniq | grep -e "[0-9]"
)
echo "Tables:" ${tables}
if [ "x${tables}" == "x" ]; then
    echo "[WARN] No subtables found!"
fi

echo -e "\nMain route table content:"
ip route list | sed -e 's/^/    /'

if [ "x${tables}" != 'x' ]; then
    for table in ${tables}; do
        echo -e "\nTable: ${table}"
        ip route list table ${table} | sed -e 's/^/    /'
    done
fi