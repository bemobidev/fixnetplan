#!/usr/bin/env python3
from distutils.command.install_data import install_data

import os
import sys
import logging
from collections import OrderedDict
import argparse
import lib.worker

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))  # os.getcwd()


def log():
    return logging.getLogger('FixNetplan')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Service for fix netplan configuration',
        epilog='(c) Bemobi'
    )
    parser.add_argument('-v', '--verbose', help='Increase output verbosity', action="store_true")
    parser.add_argument('-d', '--debug',   help='Does debug feature', action='store_true')
    parser.add_argument('-f', '--fixDhcp4',   help='Does dhcp fix', action='store_true')


    # if len(sys.argv) == 1:
    #     parser.print_help(sys.stderr)
    #     sys.exit(1)

    args = parser.parse_args()

    if args.fixDhcp4:
        fix = lib.worker.FixNetplanConfig()
        fix.fixDhcp4()
    else:
        fix = lib.worker.FixNetplanConfig()
        fix.fixEnvConfig()
        fix.run()
