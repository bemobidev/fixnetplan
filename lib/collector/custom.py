
import os
import lib.tools as tools


class Custom(object):

    def loadCloudInitData(self):
        filePath = os.path.join(tools.NETPLAN_PATH, tools.NETPLAN_CW_INPUT)
        return tools.loadYaml(filePath)

    def getSubnet(self, size=2):
        gw = self.getGW()
        _list = gw.split('.')
        sz = size
        for key in range(-3, 0):
            k = -1 * key
            if sz > 0:
                _list[k] = '0'
            sz -= 1

        sn = '%s/%d' % ('.'.join(_list), size * 8)
        return sn

