
import lib.tools as tools
import subprocess
import logging
import os
from lib.collector.custom import Custom

log = logging.getLogger('FixNetplan.collector.aws')


class Aws(Custom):

    def getEniNames(self):
        cmd = "ls /sys/class/net |  grep -oP '^ens\\d+|^eth\\d+|^wlp2s\\d+'"
        dt = self.exec(cmd)
        #         dt = """eth0
        # eth1
        # eth2
        # """
        dt = dt.strip('\n').split('\n')
        return dt

    def getEniMac(self, eni):
        data = self.exec("cat /sys/class/net/{eni}/address".format(eni=eni))
        data = data.strip('\n')
        return data

    def getIps4Mac(self, mac):
        apiUrl = '{apiBase}meta-data/network/interfaces/macs/{mac}/local-ipv4s'.format(
            apiBase=tools.BASE_API_URL,
            mac=mac
        )
        dt = tools.getUrl(apiUrl)
        if dt is not None:
            dt = dt.strip('\n').split('\n')
        else:
            dt = []

        return dt

    def getGW(self):
        cmd = 'ip route show | grep default | grep -Po "(?<=via\s)[\d\.]*" | head -n1'
        dt = self.exec(cmd)

        dt = dt.strip('\n')
        return dt

    @staticmethod
    def exec(cmd):
        # self.log.debug("Going to execute cmd: %s", cmd)

        return tools.cmdExec(cmd)

    def listRoutes(self):
        cmd = os.path.join(tools.getProjectRoot(), "routes.show.sh")

        dt = self.exec(cmd)

        dt = dt.strip('\n')
        return dt

