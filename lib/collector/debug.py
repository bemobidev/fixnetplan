
import os
import paramiko
import logging
import lib.tools as tools

from lib.collector.custom import Custom

def log():
    return logging.getLogger('FixNetplan.collector.debug')

class Debug(Custom):
    """

    """
    jbClient = None
    targetClient = None

    def __init__(self):
        """
        Init
        """
        self.log = logging.getLogger('FixNetplan.collector.debug')

        self.getSshClient()

    def loadCloudInitData(self):
        targetFilePath = os.path.join(tools.NETPLAN_PATH, tools.NETPLAN_CW_INPUT)
        if not os.path.isfile(targetFilePath):
            cmd = "cat /etc/netplan/{conf}".format(conf=tools.NETPLAN_CW_INPUT)
            dt = self.exec(cmd)

            tools.putFile(targetFilePath, data=dt)
            # self.log.debug("Loaded:\n%s", dt)

        return super().loadCloudInitData()

    @classmethod
    def getSshClient(cls):
        """
        Build ssh connection with appropriate configuration.
        :return:
        """
        logging.basicConfig()
        # Make the paramiko monolog more quiet (WARNING level)
        logging.getLogger("paramiko").setLevel(logging.WARNING)

        jumpbox_public_addr = tools.readEnvConfig('ssh_jumpbox_public_name', default='jumpbox.ncnd.fyi')
        jumpbox_private_addr = tools.readEnvConfig('ssh_jumpbox_private_name', default='jumpbox.stag.ncnd')
        target_host = tools.readEnvConfig('ssh_host', default='lba.stag.ncnd')
        user = tools.readEnvConfig('ssh_user', default='ubuntu')

        log().debug("Going to use ssh params. user: %s, host: %s, jb: %s, jb: %s",
                    user, target_host,
                    jumpbox_public_addr, jumpbox_private_addr
                    )
        ssh_key_file = tools.readEnvConfig('ssh_key_file', default='.ssh/id_rsa_4096')
        ssh_key_filepath = os.path.join(os.getenv('HOME'), ssh_key_file)

        jb_client = paramiko.SSHClient()
        jb_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        jb_client.connect(
            jumpbox_public_addr,
            username=user,
            key_filename=ssh_key_filepath
        )

        jumpbox_transport = jb_client.get_transport()
        src_addr = (jumpbox_private_addr, 22)
        dest_addr = (target_host, 22)
        jumpbox_channel = jumpbox_transport.open_channel("direct-tcpip", dest_addr, src_addr)

        target_client = paramiko.SSHClient()
        target_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        target_client.connect(
            target_host,
            username=user,
            key_filename=ssh_key_filepath,
            sock=jumpbox_channel
        )

        cls.jbClient = jb_client
        cls.targetClient = target_client

        return jb_client, target_client

    @classmethod
    def exec(cls, cmd):
        """
        Simple wrapper
        :param cmd:
        :return:
        """
        stdin, stdout, stderr = cls.targetClient.exec_command(cmd)
        return stdout.read().decode('utf-8')

    def closeSshClient(self):
        """
        Delete ssh-client resources
        :return:
        """
        self.targetClient.close()
        self.jbClient.close()

    def getEniNames(self):
        cacheKey = 'eniNames'
        if Cache.check(cacheKey):
            return Cache.get(cacheKey)

        cmd = "ls /sys/class/net |  grep -oP '^ens\\d+|^eth\\d+|^wlp2s\\d+'"
        dt = self.exec(cmd)

        dt = dt.strip('\n').split('\n')
        Cache.set(cacheKey, dt)
        return dt

    def getEniMac(self, eni):
        cacheKey = 'mac4eni_%s' % eni
        if Cache.check(cacheKey):
            return Cache.get(cacheKey)

        cmd = "cat /sys/class/net/{eni}/address".format(eni=eni)
        dt = self.exec(cmd)

        dt = dt.strip('\n')
        Cache.set(cacheKey, dt)
        return dt

    def getIps4Mac(self, mac):
        cacheKey = 'ip4mac_%s' % mac
        if Cache.check(cacheKey):
            return Cache.get(cacheKey)

        apiUrl = '{apiBase}meta-data/network/interfaces/macs/{mac}/local-ipv4s'.format(
            apiBase=tools.BASE_API_URL,
            mac=mac
        )
        # self.log.debug("Going to request URL: %s", apiUrl)
        cmd = 'curl -s {url}'.format(url=apiUrl)
        dt = self.exec(cmd)

        dt = dt.strip('\n').split('\n')
        Cache.set(cacheKey, dt)
        return dt

    def getGW(self):
        if Cache.check('gw'):
            return Cache.get('gw')
        cmd = 'ip route show | grep default | grep -Po "(?<=via\s)[\d\.]*" | head -n1'
        dt = self.exec(cmd)

        dt = dt.strip('\n')
        Cache.set('gw', dt)
        return dt

    def listRoutes(self):
        cmd = "/opt/fixnetplan/routes.show.sh"
        dt = self.exec(cmd)

        dt = dt.strip('\n')
        return dt

    def __del__(self):
        self.log.warning("Destructor executed")
        self.closeSshClient()


class Cache:

    _data = None

    @classmethod
    def check(cls, name):
        cls._load()
        if name in cls._data:
            return True
        return False

    @classmethod
    def set(cls, name, value):
        cls._data[name] = value

        cls._store()

    @classmethod
    def get(cls, name, default=None):
        if cls.check(name):
            return cls._data[name]
        return default

    @classmethod
    def _store(cls):
        path = os.path.join(tools.getProjectRoot(), 'cache.yaml')
        tools.putYamlFile(path, cls._data)

    @classmethod
    def _load(cls):
        path = os.path.join(tools.getProjectRoot(), 'cache.yaml')
        cls._data = tools.loadYaml(path, default={})

