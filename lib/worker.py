
import os
import logging
from collections import OrderedDict

import time
import getpass

import lib.tools as tools
from lib.collector.aws import Aws as AwsCollector
from lib.collector.debug import Debug as DebugCollector
from lib.storage import Storage

def log():
    return logging.getLogger('FixNetplan.lib.worker')



class FixNetplanConfig(object):
    """
    https://netplan.io/examples

    """
    _config = None

    DEBUG = True

    log = None

    _collector = None

    def __init__(self):
        # tFormat = '%(asctime)s [%(levelname)s] %(name)s -> %(message)s'
        # tFormat = '%(name)s [%(levelname)s] \t -> %(message)s'
        tFormat = '[%(levelname)s] \t -> %(message)s'

        logging.basicConfig(level=logging.DEBUG,
                            format=tFormat)
        self.log = logging.getLogger('FixNetplan')

        self.DEBUG = tools.DEBUG = tools.readEnvConfig('debug', False)

        # InProject netplan folder for debug mode
        tools.NETPLAN_PATH = '/etc/netplan/'
        if self.DEBUG:
            tools.NETPLAN_PATH = os.path.join(tools.getProjectRoot(), 'netplan.d')
            tools.NETPLAN_CW_OUTPUT = '50-cloud-init.[upd].yaml'

        self.log.debug("Going to use folder as netplan: %s", tools.NETPLAN_PATH)

        self.data = {}
        self.additionalData = {}
        self.enisInfo = None

    @classmethod
    def collector(cls):
        if cls._collector is not None:
            return cls._collector

        if tools.checkInAws() or tools.readEnvConfig('collector', default='aws') == 'aws':
            cls._collector = AwsCollector()
        else:
            cls._collector = DebugCollector()

        log().debug("Going to use data-collector: %r", cls._collector.__class__)
        return cls._collector

    def fixEnvConfig(self):
        if tools.checkEnvConfigExists():
            self.log.debug("Env config already exists")
            return

        inAws = True
        iid = tools.getCurrentInstanceId()
        if iid is None:
            self.log.critical("IID not found. Looks like local start.")
            inAws = False
            iid = tools.getCurrentInstanceIdOverSsh()
        self.log.debug("IID found: %r", iid)

        region = tools.getInstanceRegion()
        if region is None:
            self.log.critical("Region not found. Looks like local start.")
            inAws = False
            region = tools.getInstanceRegionOverSsh()
        self.log.debug("Region found: %r", region)

        if not inAws:
            targetEnvName = 'env.debug.yaml'
            if os.path.isfile(tools.getEnvConfigPath(targetEnvName)):
                self.log.debug("Going to use debug config file: %s. Going to create symlink",
                               tools.getEnvConfigPath(targetEnvName))
                os.symlink(tools.getEnvConfigPath(targetEnvName), tools.getEnvConfigPath())
        else:
            environmentTagName = tools.readMainConfig('environmentTagName', 'environment')
            environment = tools.getAwsInstanceTags3(region, iid, environmentTagName)
            targetEnvName = 'config/env.{environment}.yaml'.format(environment=environment)
            if os.path.isfile(tools.getEnvConfigPath(targetEnvName)):
                self.log.debug("Found config file: %s. Going to create symlink", tools.getEnvConfigPath(targetEnvName))
                os.symlink(tools.getEnvConfigPath(targetEnvName), tools.getEnvConfigPath())
            else:
                self.log.warning("No appropriate config found. Should downgrade to default.")
                # TODO: Do add default config source

    def run(self):
        self.data = self.collector().loadCloudInitData()

        domainSearchPath = 'network.ethernets.*.nameservers.search'
        searchDomains = [
            tools.readEnvConfig('privateIntDomain'),
            tools.readEnvConfig('privateAwsDomain')
        ]
        pathChanged = tools.checkPathExists(self.data, domainSearchPath, default=searchDomains)

        self.enisInfo = self.collectEnisInfo()
        self.fixEnis()

        Storage.addToStoreQueue(tools.NETPLAN_CW_INPUT, self.data)

        foundChanges = Storage.checkStoreQueue()

        # FIXME: pathChanged seems overproof
        if foundChanges or pathChanged:
            if self.DEBUG:
                self.log.info("netplan apply could be here")
            else:
                tools.netplanApply()

                time.sleep(10)
        else:
            self.log.info("Nothing to apply by netplan service")

        # FYIO: Show routes after apply config!!!
        routes = self.collector().listRoutes()
        self.log.info("Result routes list:\n%s", routes)

    @classmethod
    def collectEnisInfo(cls):
        """
        Collects information about present enis
        :return:
        """
        data = {}
        enis = cls.collector().getEniNames()
        # self.log.debug("Found enis: %r", enis)

        for eni in enis:
            item = {}
            item['mac'] = mac = cls.collector().getEniMac(eni)
            # self.log.debug("[%r] -> mac: %r", eni, mac)

            item['ips'] = ips = cls.collector().getIps4Mac(mac)
            # self.log.debug("Found ips: %r", ips)
            data[eni] = item

        log().info("Collected data: \n%s", tools.toYaml(data))
        return data

    def fixEnis(self):
        """
        Does fixes
        :return:
        """
        if len(self.data['network']['ethernets']) == 1:
            self.log.warning("Found only one ethernet eni. Going to ignore re-configuration ")
            return

        i = 0
        for key in self.data['network']['ethernets']:
            if key.startswith('eth') \
                    or key.startswith('ens') \
                    or key.startswith('wlp2s'):
                routes = []
                dhcp4 = i in tools.readMainConfig('DHCP_ENABLED_FOR_ENI_NUM', [1])
                PRIVATE_ROUTE_ON_ENI_NUM = tools.readMainConfig('PRIVATE_ROUTE_ON_ENI_NUM', 0)

                if i == PRIVATE_ROUTE_ON_ENI_NUM:
                    self.log.debug('Adding route for private network: %r', key)
                    routes = [
                        OrderedDict({
                            'to': self.collector().getSubnet(2),      # 'to': '172.25.0.0/16',
                            'via': self.collector().getGW(),
                            'metric': 200
                        }),
                    ]

                self.buildConfigForEni(
                    key, i,
                    dhcp4=dhcp4,
                    routes=routes
                )

                i += 1
        return None

    @staticmethod
    def generateRouteTableName(idx):
        return 100 + idx * 1000

    def buildConfigForEni(self, eniName, idx, dhcp4=False, routes=[]):
        """
        Builds configuration for non primary enis
        :param routes:
        :param dhcp4:
        :param eniName:
        :param idx:
        :return:
        """
        self.log.debug("Going to build config for Eni: %s", eniName)

        # Updating main config data
        self.data['network']['ethernets'][eniName]['dhcp4'] = dhcp4
        self.data['network']['ethernets'][eniName]['dhcp6'] = False

        # Create own config
        routeTable = self.generateRouteTableName(idx)
        addData = self.data['network']['ethernets'][eniName].copy()
        addData.pop('dhcp4-overrides', None)
        addData['routes'] = routes
        addData['routes'].append(
            OrderedDict({
                'to': '0.0.0.0/0',
                'via': self.collector().getGW(),
                'table': routeTable,
            })
        )
        addData['routing-policy'] = []

        self.additionalData[eniName] = addData

        for ip in self.enisInfo[eniName]['ips']:
            ipWMask = ip + '/24'

            if 'addresses' not in addData:
                addData['addresses'] = []

            self.log.debug("Going to check if IP(%r) in a list: %r", ipWMask, addData['addresses'])
            if ipWMask not in addData['addresses']:
                self.log.debug("Going to add ip-mask: %s", ipWMask)
                addData['addresses'].append(ipWMask)
            else:
                self.log.debug("It's ok with ip: %s", ipWMask)

            # src based rotes
            addData['routes'].append(
                OrderedDict({
                    'to': ip,
                    'via': '0.0.0.0',
                    'scope': 'link',
                    'table': routeTable
                })
            )
            addData['routing-policy'].append(
                OrderedDict({
                    'from': ip,
                    'table': routeTable
                })
            )

        self.log.debug("Additional Routes:\n%s", tools.toYaml(addData['routes']))
        self.log.debug("Additional RoutingPolicy:\n%s", tools.toYaml(addData['routing-policy']))

        configFileName = eniName + '.yaml'
        configFilePath = os.path.join(tools.NETPLAN_PATH, configFileName)
        storeData = {
            'network': {
                'version': 2,
                'ethernets': {eniName: self.additionalData[eniName]},
            }
        }
        # tools.putYamlFile(configFilePath, storeData)
        Storage.addToStoreQueue(configFileName, storeData)

    @classmethod
    def addToStoreQueue(cls, file, data):
        originFile = os.path.join(tools.NETPLAN_PATH, file)

        yamlData = tools.toYaml(data)
        # self.log.info("File: %s, \n%s", file, yamlData)
        cls.keepFiles[file] = {
            'data': data,
            'new_md5': tools.getStringMd5(yamlData.encode('utf-8')),
            'origin_md5': tools.getFileMd5(originFile)
        }


    @classmethod
    def fixDhcp4(cls):
        data = cls.collector().loadCloudInitData()
        if len(data['network']['ethernets']) == 1:
            log().warning("Found only one ethernet eni. Going to ignore dhcp4 re-configuration ")
            return

        # self.collectEnisInfo()
        log().info("Going to fix dhcp4.")
        i = 0
        for key in data['network']['ethernets']:
            if key.startswith('eth') \
                    or key.startswith('ens') \
                    or key.startswith('wlp2s'):
                dhcp4 = i in tools.readMainConfig('DHCP_ENABLED_FOR_ENI_NUM', [1])
                log().info("For %s dhcp4 is: %r", key, dhcp4)
                # Updating main config data
                data['network']['ethernets'][key]['dhcp4'] = dhcp4

                i += 1

        Storage.addToStoreQueue(tools.NETPLAN_CW_INPUT, data)
        foundChanges = Storage.checkStoreQueue()

        if foundChanges:
            if getpass.getuser() == 'root':
                tools.netplanApply()
                time.sleep(10)
            else:
                log().critical("Only root is able to apply netplan")
        else:
            log().info("Nothing to apply by netplan service")