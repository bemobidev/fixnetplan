
import os
import logging
import lib.tools as tools


def log():
    return logging.getLogger('FixNetplan.lib.storage')


class Storage:
    keepFiles = {}

    @classmethod
    def addToStoreQueue(cls, file, data):
        originFile = os.path.join(tools.NETPLAN_PATH, file)

        yamlData = tools.toYaml(data)
        # self.log.info("File: %s, \n%s", file, yamlData)
        cls.keepFiles[file] = {
            'data': data,
            'new_md5': tools.getStringMd5(yamlData.encode('utf-8')),
            'origin_md5': tools.getFileMd5(originFile)
        }

    @classmethod
    def checkStoreQueue(cls):
        foundChanges = False

        for configFileName in cls.keepFiles:
            item = cls.keepFiles[configFileName]
            if item['new_md5'] == item['origin_md5']:
                log().info("Config file: %30s \t - Not changed", configFileName)
            else:
                foundChanges = True

                if configFileName == tools.NETPLAN_CW_INPUT:
                    configFileName = tools.NETPLAN_CW_OUTPUT
                log().info("Config file: %30s \t - Changed!!", configFileName)

                configFilePath = os.path.join(tools.NETPLAN_PATH, configFileName)
                log().info("Going to store updated config: %s", configFilePath)
                tools.putYamlFile(configFilePath, item['data'])

        return foundChanges
