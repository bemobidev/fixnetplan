
import os
import logging
import json
import yaml
import urllib.request
import hashlib
import boto3
import subprocess

from collections import OrderedDict


def log():
    return logging.getLogger('FixNetplan.lib.tools')


DEBUG = True
NETPLAN_PATH = '/etc/netplan/'
NETPLAN_CW_INPUT = '50-cloud-init.yaml'
NETPLAN_CW_OUTPUT = '50-cloud-init.yaml'


ENV_CONFIG_FILE_NAME = 'env.yaml'


def checkPathExists(data, path, default=None):
    """
    Checks if path exists and has appropriate value or default
    :param path:
    :param default:
    :return:
    """
    log().debug("Going to check path: %s. With value: %r", path, default)
    parts = path.split('.')
    if default is None:
        default = []

    pathChanged = False
    lastPartData = data
    for index, part in enumerate(parts):

        keys = lastPartData.keys()
        if part == '*':
            part = list(keys)[0]

        if part in lastPartData.keys():
            lastPartData = lastPartData[part]
        else:
            pathChanged = True
            if index < len(parts) - 1:
                lastPartData[part] = {}
            else:
                lastPartData[part] = default

            lastPartData = lastPartData[part]
    return pathChanged


def cmdExec(cmd):
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    proc.wait()
    dt = proc.stdout.read().decode("utf-8")

    result = None
    if proc.returncode != 0:
        log().critical("Got error code: %s", proc.returncode)

    return dt


def toJson(data):
    return json.dumps(data, indent=4)


def represent_ordereddict(dumper, data):
    value = []

    for item_key, item_value in data.items():
        node_key = dumper.represent_data(item_key)
        node_value = dumper.represent_data(item_value)

        value.append((node_key, node_value))

    return yaml.nodes.MappingNode(u'tag:yaml.org,2002:map', value)


def toYaml(data):
    """
    Converts data to yaml-structure string.
    FYIO: Has representation for OrderedDict.
    :param data:
    :return:
    """
    yaml.add_representer(OrderedDict, represent_ordereddict)

    return yaml.dump(
        data,
        default_flow_style=False,
        allow_unicode=True
    )


def loadYaml(filePath, default=None):
    """
    Loads yaml config from file
    :return:
    :param filePath: string
    :param default:
    """
    data = default
    # log().info("Going to load yaml: %r", filePath)
    if not os.path.exists(filePath):
        return data

    with open(filePath, 'r+') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)

    return data


def getUrl(url):
    html = None
    try:
        with urllib.request.urlopen(url) as response:
            html = response.read().decode('utf-8')
        # self.log.debug("HTML : %r", html)
    except (OSError) as e:
        log().error("Execption appear: %r", e)

    return html


def putFile(fileName, data):
    """
    Just put file or overwrite
    :param fileName:
    :param data:
    :return:
    """
    # log.debug("Going to save file: %r with data: \n%s", fileName, data)
    # log().info('Saving configuration: %r', fileName)
    with open(fileName, 'w+') as f:

        # log.debug("Going to write to target netplan config file")
        f.seek(0)
        f.truncate(0)
        f.write(data)


def putYamlFile(fileName, data):
    yamlData = toYaml(data)
    return putFile(fileName, yamlData)


def getEnvConfigPath(confName=None):
    if confName is None:
        confName = ENV_CONFIG_FILE_NAME
    return os.path.join(getProjectRoot(), confName)


def checkEnvConfigExists():
    configPath = getEnvConfigPath()

    return os.path.exists(configPath)


def readMainConfig(key, default=None):
    if "config" not in readMainConfig.__dict__:
        readMainConfig.config = None

    if readMainConfig.config is None:
        customConfPath = getEnvConfigPath(confName='config/config.yaml')
        log().debug('Checking custom base config exists: %s', customConfPath)
        if os.path.exists(customConfPath):
            log().debug('Found custom base config: %s', customConfPath)
            readMainConfig.config = loadYaml(customConfPath)
        else:
            defaultConfPath = getEnvConfigPath(confName='config.default.yaml')
            log().debug('No custom base config found. Going to use default: %s', defaultConfPath)
            readMainConfig.config = loadYaml(defaultConfPath)

    if key in readMainConfig.config.keys():
        log().debug('Loading key: %s = %r', key, readMainConfig.config[key])
        return readMainConfig.config[key]

    return default


def readEnvConfig(key, default=None):
    if "config" not in readEnvConfig.__dict__:
        readEnvConfig.config = None

    if readEnvConfig.config is None:
        configPath = getEnvConfigPath()
        if checkEnvConfigExists():
            readEnvConfig.config = loadYaml(configPath)
        else:
            log().critical(
                "No config file found: %s when looking for key: %s. Returning Default: %s",
                configPath, key, default
            )
            return default

    if key in readEnvConfig.config.keys():
        return readEnvConfig.config[key]
    else:
        return default


def getProjectRoot():
    PROJECT_ROOT = os.path.realpath(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            '..'
        )
    )
    return PROJECT_ROOT


def getStringMd5(data):
    m = hashlib.md5()
    m.update(data)
    return m.hexdigest()


def getFileMd5(file):
    m = hashlib.md5()
    if os.path.isfile(file):
        with open(file, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                m.update(chunk)
    else:
        m.update(''.encode('utf-8'))
    return m.hexdigest()


# def getAwsInstanceTag(name, default=None):
#     region = "us-east-2"
#     import boto.ec2
#
#     conn = boto.ec2.connect_to_region(region)
#     reservations = conn.get_all_instances()
#     for res in reservations:
#         for inst in res.instances:
#             if 'Name' in inst.tags:
#                 log().debug("Inst: %s (%s) [%s]", inst.tags['Name'], inst.id, inst.state)
#             else:
#                 log().debug("Inst: %s [%s]", inst.id, inst.state)


def getAwsInstanceTags3(region, iid, name, default=None):
    logging.getLogger('boto3').setLevel(logging.INFO)
    logging.getLogger('botocore').setLevel(logging.INFO)
    logging.getLogger('nose').setLevel(logging.INFO)

    client = boto3.client("ec2", region_name=region)
    data = client.describe_tags(
        DryRun=False,
        Filters=[
            {
                'Name': 'resource-id',
                'Values': [
                    iid,
                ],
            },
            {
                'Name': 'key',
                'Values': [name],
            },
        ],
        MaxResults=10,
    )

    result = default

    log().debug("Response tags data: \n%s", toYaml(data['Tags']))
    if len(data['Tags']) > 0:
        result = data['Tags'][0]['Value']
        log().debug("Found tag [%s]: %s", name, result)

    else:
        log().error("No tag found: %s", name)

    return result

# BASE_API_URL = "http://instance-data/latest/"
BASE_API_URL = 'http://169.254.169.254/latest/'


def checkInAws():
    dt = getUrl(BASE_API_URL)

    if dt is None:
        log().debug('Not in aws')
        return False

    return True


def getCurrentInstanceId():
    apiUrl = '{apiBaseUrl}meta-data/instance-id'.format(apiBaseUrl=BASE_API_URL)

    dt = getUrl(apiUrl)
    if dt is not None:
        dt = dt.strip('\n')
        log().debug("Instance ID found: %s", dt)

    return dt


def getCurrentInstanceIdOverSsh():
    apiUrl = '{apiBaseUrl}meta-data/instance-id'.format(apiBaseUrl=BASE_API_URL)

    from lib.collector.debug import Debug as DebugCollector
    DebugCollector.getSshClient()
    dt = DebugCollector.exec("curl -s %s" % apiUrl)
    return dt


def getInstanceRegion():
    """
    Looks for region name of given instance
    :return:
    """
    apiUrl = '{apiBaseUrl}dynamic/instance-identity/document' \
        .format(apiBaseUrl=BASE_API_URL)

    dt = getUrl(apiUrl)
    if dt is not None:
        dt = json.loads(dt)
        dt = dt['region']

    return dt


def getInstanceRegionOverSsh():
    """
    Looks for region name of given instance over ssh
    :return:
    """
    apiUrl = '{apiBaseUrl}dynamic/instance-identity/document' \
        .format(apiBaseUrl=BASE_API_URL)
    from lib.collector.debug import Debug as DebugCollector
    DebugCollector.getSshClient()
    dt = DebugCollector.exec("curl -s %s " % apiUrl)

    if dt is not None:
        dt = json.loads(dt)
        dt = dt['region']

    return dt


def netplanApply():
    """
    Does apply of netplan configuration
    :return:
    """
    log().debug("Going to apply netplan")
    dt = cmdExec('netplan apply')
    log().info("Netplan apply result:\n%s", dt)

