# README #

This repository contains fixnetplan service. This service does fix and update of 
'nameservers -> search' section and do configuration of additional interfaces (ENIs)

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

\# route -n  
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         172.25.1.1      0.0.0.0         UG    100    0        0 eth0
172.25.1.0      0.0.0.0         255.255.255.0   U     0      0        0 eth0
172.25.1.1      0.0.0.0         255.255.255.255 UH    100    0        0 eth0

\# ip route show  
default via 172.25.1.1 dev eth0 proto dhcp src 172.25.1.200 metric 100 
172.25.1.0/24 dev eth0 proto kernel scope link src 172.25.1.200 
172.25.1.1 dev eth0 proto dhcp scope link src 172.25.1.200 metric 100 

ubuntu@ip-172-25-1-200:~$ route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         172.25.1.1      0.0.0.0         UG    100    0        0 eth0
172.25.1.0      0.0.0.0         255.255.255.0   U     0      0        0 eth2
172.25.1.0      0.0.0.0         255.255.255.0   U     0      0        0 eth1
172.25.1.0      0.0.0.0         255.255.255.0   U     0      0        0 eth0
172.25.1.1      0.0.0.0         255.255.255.255 UH    100    0        0 eth0
ubuntu@ip-172-25-1-200:~$ ip route list
default via 172.25.1.1 dev eth0 proto dhcp src 172.25.1.200 metric 100 
172.25.1.0/24 dev eth2 proto kernel scope link src 172.25.1.204 
172.25.1.0/24 dev eth1 proto kernel scope link src 172.25.1.202 
172.25.1.0/24 dev eth0 proto kernel scope link src 172.25.1.200 
172.25.1.1 dev eth0 proto dhcp scope link src 172.25.1.200 metric 100 



* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
