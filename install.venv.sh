#!/usr/bin/env bash

# Default Declaration #########
GREADLINK=`which greadlink`
if [[ "$GREADLINK" = "" ]]; then
    EXEC_PATH=$(dirname "$(readlink -f "$0")")
else
    EXEC_PATH=$(dirname "$(greadlink -f "$0")")
fi

PROJECT_ROOT=$(echo "$EXEC_PATH" | xargs realpath)
echo "Project Root: ${PROJECT_ROOT}"

PROFILE="${HOME}/.bashrc"
if [ "$(uname)" == 'Linux' ]; then
    PROFILE="${HOME}/.bashrc"
else
    PROFILE="${HOME}/.profile"
fi

if [ ! -f "${PROFILE}" ]; then
    echo "[INFO] Going to create file ${PROFILE}"
    touch "${PROFILE}"
fi

function install_asdf() {
    if [ -d  "$HOME/.asdf/" ]; then
      echo "[INFO] asdf already installed. Rejecting"
      return
    fi

    echo "[INFO] Initialising asdf tool"
    LAST_PATH=$(pwd)
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf
    cd ~/.asdf
    git checkout "$(git describe --abbrev=0 --tags)"

    # echo -e '\n. $HOME/.asdf/asdf.sh' >> ${PROFILE}
    echo -e '\n. $HOME/.asdf/completions/asdf.bash' >> ${PROFILE}
}

function install_direnv() {
    #
    # https://github.com/asdf-community/asdf-direnv#setup
    #
    asdf plugin-add direnv

    asdf install direnv 2.20.0

    asdf global  direnv 2.20.0

    asdf exec direnv allow .envrc

    if ! grep '\.asdf\/bin\:\$PATH' "${PROFILE}" ; then
        echo -e '\nexport PATH="$HOME/.asdf/bin:$PATH" ' >> "${PROFILE}"
    fi

    if ! grep 'asdf exec direnv hook bash' "${PROFILE}"; then
        echo -e '# Hook direnv into your shell.' >> "${PROFILE}"
        echo -e 'eval "$(asdf exec direnv hook bash)"' >> "${PROFILE}"
    fi

    # Global asdf-direnv integration.
    # The ~/.config/direnv/direnvrc file is a good place to add common functionality for all .envrc file.
    # The following snippet makes the use asdf feature available:

    DIRENVRC="$HOME/.config/direnv/direnvrc"
    if [ ! -f "${DIRENVRC}" ]; then
        echo "[INFO] direnvrc does not exists. Going to create"
        touch "${DIRENVRC}"
        echo -e 'source $(asdf which direnv_use_asdf) \n\n' >> "${DIRENVRC}"
        echo -e '# Uncomment the following line to make direnv silent by default.' >> "${DIRENVRC}"
        echo -e '# export DIRENV_LOG_FORMAT=""' >> "${DIRENVRC}"
    fi

}

function install_virtualenv() {
    if hash virtualenv; then
      echo "[INFO] virlualenv already installed. Rejecting."
    elif [ "$(uname)" == 'Linux' ]; then
      echo "[INFO] Going to install";
      sudo apt-get install virtualenv
    else
      #      port install virtualenv
      #      brew install virtualenv

      sudo pip3 install virtualenv
    fi
}

function prepare_virtualenv() {
    VENV_DIR="${PROJECT_ROOT}/venv"
    if [ -d "${VENV_DIR}" ]; then
        echo "[INFO] venv config exists. Installation rejected."
    else
        echo "[INFO] No venv dir exists ${VENV_DIR}. Going to install."
        virtualenv -p /usr/bin/python3 venv
    fi
}



install_virtualenv

prepare_virtualenv

install_asdf

install_direnv


pip3 --version
which pip3

